/**

 **/

#include <iostream>
#include <fstream>
#include "mpi.h"

using namespace std;

#include <bilinearEval.h>
#include <ms-search.h>
#include <ms-worker.h>

/***
 *     Main 
 *
 *
 * Compile from code04 directory:
 *
 * mkdir build
 * cd build
 * mpic++ -o master-workers -std=c++11 -I. -I../src ../src/master-workers.cpp
 *
 *
 * To execute:
 *
 * mpirun -np 4 ./master-workers 2
 *
 ***/
int main(int argc, char ** argv) {

  int numtasks, len;
  char hostname[MPI::MAX_PROCESSOR_NAME];
  int rank ;

  MPI::Init(argc, argv);
  numtasks = MPI::COMM_WORLD.Get_size();
  rank = MPI::COMM_WORLD.Get_rank();
  MPI::Get_processor_name(hostname, len);

    // Evaluation function
  BilinearEval eval;

  if (rank == 0) {
      int size = 57;
    
//      cout << "----Master says hello----" << endl;
      MPI::COMM_WORLD.Bcast(&size, 1, MPI::INT, 0);

      int cores = numtasks - 1;

      unsigned duration = atoi(argv[1]);

      MS_SEARCH master(57, cores, duration);
      master.run();
    
  } else {
//    cout << "----Worker " << rank << " says hello----" << endl;
    msWorker<Solution<unsigned int> > worker(eval);
    worker.run();
  }

//  std::cout << "Finalization of " << rank << ", end." << std::endl;
  MPI::Finalize();
  
  return 0;
}
