#ifndef __Eval
#define __Eval

template<class SolT>
class Eval {
public:
    virtual void operator()(SolT & _x) = 0;

};

#endif